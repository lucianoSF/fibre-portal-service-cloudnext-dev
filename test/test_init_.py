import sys
sys.path.append('..')
import unittest
from app import models
from unittest.mock import patch


class TestInit(unittest.TestCase):
	'''
	def setUp(self):
'''

	@patch("app.models.populate_db")
	@patch("app.models.connect")
	def test_model_init_app_mock(self, populate_mock, connect_mock):
		from flask import Flask
		from flask_cors import CORS
		aplication = Flask(__name__)
		CORS(aplication,  resources={r"/*": {"origins": "*"}})
		models.init_app(aplication)
		populate_mock.assert_called()
		connect_mock.assert_called()



if __name__ == '__main__':
    unittest.main()
    