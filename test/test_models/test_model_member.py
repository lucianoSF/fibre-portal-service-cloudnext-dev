import sys
sys.path.append('..')
import unittest
import app
from test import data_test

class TestModelMember(unittest.TestCase):
	def setUp(self):
		app.create_app()
		self.member = app.models.member.Member()


	'''def test_model_member_get_all(self):
		members = self.member.get_all()
		self.assertEqual(members.count(), 1)
		self.assertEqual(str(type(members)), "<class 'mongoengine_goodjson.queryset.QuerySet'>")
	'''

	def test_model_member_get_not_found(self):
		with self.assertRaises(app.rest.errors.NotFoundError):
			self.member.get("000000000000000000000000")

	def test_model_member_get_found(self):
		member = self.member.get(data_test.MEMBER_ID)
		self.assertEqual(str(member.id), data_test.MEMBER_ID)
		self.assertEqual(str(type(member)), "<class 'app.models.member.Member'>")

	def test_model_member_get_ssh_keys(self):
		member = self.member.get_ssh_keys(data_test.MEMBER_ID)
		self.assertEqual(str(member.id), data_test.MEMBER_ID)
		self.assertEqual(str(type(member)), "<class 'app.models.member.Member'>")



if __name__ == '__main__':
    unittest.main()
    
