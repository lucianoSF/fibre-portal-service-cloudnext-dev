import requests
import json

headers = {
	'Content-Type':'application/json'
}
servidor = 'http://172.17.0.1:5000/'


def root_login():
	root = requests.post(servidor + 'auth/login', 
			headers = headers,
			data = json.dumps({"email": "root@portalservice.com","password": "root_pass"}))
	return root.json()

'''def create_image(root_id):
	image = requests.post(servidor + 'images/create', 
			headers = {'Content-Type':'application/json', 'Authorization': root_id},
			data = json.dumps({"name": "image1","attributes": {"att1":"d1", "att2":"d2"}}))
	return image.json()'''

def create_member(root_id):
	image = requests.post(servidor + 'members/create', 
			headers = {'Content-Type':'application/json', 'Authorization': root_id},
			data = json.dumps({"first_name": "luciano","last_name": "fraga","affiliation": "inf.ufg","email": "lucianofraga@mail.com","password": "12345678","country": "test","city": "test"}))
	return image.json()


#print(login)
#print(create_image(login['result']['auth_token']))


if __name__ == '__main__':
	login = root_login()
	#image = create_image(login['result']['auth_token'])
	member = create_member(login['result']['auth_token'])
	print(login)
	#print(image)
	print(member)
	f = open("test/data_test.py", "w")
	f.write("ROOT_ID = '" + login['result']['auth_token'] + "'\n")
	#f.write("IMAGE_ID = '" + "111111111111111111111111" + "'\n")
	f.write("MEMBER_ID = '" + member['result']['id'] + "'")
	f.close()
	
