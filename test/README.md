## CloudNEXT Testes Funcionais
Execução de testes funcionais e visualização da documentação

### + Executando os testes localmente
Para a execução dos testes de maneira manual, é preciso executar a aplicação em modo `HTTP`, que pode ser
feito através da variável de ambiente `HTTP_MODE=1`.

Talvez seja necessário a mudança do endereço do servidor em `test/populate_test_objects.py`.

Exemplo:
```
  headers = {
    'Content-Type':'application/json'
  }
  servidor = 'http://0.0.0.0:8000/api/'
```
#### - Executando os testes
```
docker exec -it backend python3 -m pytest -v
```


### + Documentação
#### - Páginas HTML
O comando a seguir cria as páginas html dentro do container da aplicação:
```
docker exec -it backend pdoc test/ --html
```
Em seguida para copiar os arquivos gerados para a pasta test_doc no diretório atual:
```
docker cp backend:app/html/test test_doc
```

#### - Servidor HTTP
Para visualizar a documentação em um servidor http, é preciso adicionar uma porta de acesso
ao container em `docker-compose.yaml`.

Exemplo:
```
   ports: 
      - 5000:8000
      - 1234:1234   
```
O comando a seguir cria o servidor na porta indicada:
```
docker exec -it backend pdoc test/ --http 0.0.0.0:1234
```
O servidor pdoc estará disponivel no endereço indicado.

Exemplo:
```
http://0.0.0.0:1234
```
