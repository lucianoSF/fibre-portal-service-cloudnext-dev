import os, logging

# db config
db_host = str(os.getenv("MONGODB_HOST"))
db_port = str(os.getenv("MONGODB_PORT"))
db_user = str(os.getenv("MONGODB_USER"))
db_pass = str(os.getenv("MONGODB_PASSWORD"))
db_dbase = str(os.getenv("MONGODB_DATABASE"))

# jwt config
jwt_secret_key = str(os.getenv("JWT_SECRET_KEY"))
jwt_algorithm = str(os.getenv("JWT_ALGORITHM"))
jwt_exp_days = int(str(os.getenv("JWT_EXPIRATION_DAYS")))
jwt_exp_seconds = int(str(os.getenv("JWT_EXPIRATION_SECONDS")))

# app config
_log_levels = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL,
    '': logging.NOTSET
}
log_level = _log_levels.get(str(os.getenv("LOG_LEVEL")))
root_login = str(os.getenv("ROOT_LOGIN"))
root_pwd = str(os.getenv("ROOT_PASSWORD"))
