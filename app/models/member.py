from mongoengine_goodjson import Document, EmbeddedDocument
from mongoengine import signals, StringField, BooleanField, IntField, EmbeddedDocumentField
from ..rest.errors import NotFoundError, BadRequestError, NotAuthorizedError
from OpenSSL import crypto
import bcrypt, json, sys


class SSHKeys(EmbeddedDocument):
    private_key = StringField()
    public_key = StringField()

class Member(Document):
    first_name = StringField()
    last_name = StringField()
    affiliation = StringField()
    email = StringField(unique=True)
    password = StringField() 
    country = StringField()
    city = StringField()
    privilege = IntField(default=1)
    enabled = BooleanField(default=True)
    ssh_keys = EmbeddedDocumentField(SSHKeys)

    @classmethod
    def post_save(cls, sender, document, **kwargs):
        document.password = None

    @staticmethod
    def get_all():
        return Member.objects().exclude('password').exclude('ssh_keys')

    @staticmethod
    def get(id):
        member = Member.objects(id=id).exclude('password').exclude('ssh_keys').first()
        if not member: raise NotFoundError(f"Member with id = '{id}'not found")
        return member
    
    @staticmethod
    def get_ssh_keys(id):
        member = Member.objects(id=id).only('ssh_keys').first()
        if not member: raise NotFoundError(f"Member with id = '{id}'not found")
        return member

    @staticmethod
    def hash_string(string):
        return (bcrypt.hashpw(string.encode('utf-8'), bcrypt.gensalt())).decode('utf-8')
    
    @staticmethod
    def validate_create(member_dict):
        if not 'password' in member_dict: raise BadRequestError("You must create a password (min 8 characters)")
        if len(member_dict['password']) < 5: raise BadRequestError("Password must have at least 8 characters")
        member_dict['password'] = Member.hash_string(member_dict['password'])
        member_dict['privilege'] = 1
        member_dict['ssh_keys'] = json.loads(Member.generate_ssh_keys().to_json())
        return member_dict


    @staticmethod
    def validate_update(member_dict):
        if 'password' in member_dict:
             if len(member_dict['password']) < 5: raise BadRequestError("Password must have at least 8 characters")
             member_dict['password'] = Member.hash_string(member_dict['password'])
        return member_dict
    
    @staticmethod
    def update_fields(member_id, fields_dict):
        member = Member.get(member_id)
        member.update(**Member.validate_update(fields_dict))
        member.save()
        member.reload()
        return Member.get(member_id)
    
    @staticmethod
    def change_password(member_id, old_pwd, new_pwd):
        member = Member.objects(id=member_id).only('password').first()
        if not member: raise NotFoundError(f"Member with id = '{member_id}'not found")
        if not bcrypt.checkpw(old_pwd.encode('utf-8'), member.password.encode('utf-8')): raise NotAuthorizedError("Password does not match.")
        Member.objects(id=member_id).update(set__password=Member.hash_string(new_pwd))

    @staticmethod
    def projects(member_id):
        Member.get(member_id)
        Project = getattr(sys.modules[__package__ + '.project'], "Project")
        participants_roles = getattr(sys.modules[__package__ + '.project'], "participants_roles")
        projects = []
        def _format(role, list):
            for p in list:
                p_dict = json.loads(p.to_json())
                p_dict["role"] = role
                p_dict["owner"] = json.loads((Member.get(p_dict["owner"])).to_json())
                p_dict.pop('participants', None)
                projects.append(p_dict)
        
        _format("owner", Project.objects(owner=member_id).exclude("participants"))
        for role in participants_roles:
            _format(role, Project.objects(participants__match={'role': role, 'member': member_id}).exclude("participants"))
        
        return projects

    @staticmethod
    def requested_projects(id):
        Project = getattr(sys.modules[__package__ + '.project'], "Project")
        return Project.objects(requested_accesses__member_id=id).exclude("participants")
    
    @staticmethod
    def generate_ssh_keys():
        ssh_keys = SSHKeys()
        k = crypto.PKey()
        k.generate_key(crypto.TYPE_RSA, 4096)
        ssh_keys.private_key = crypto.dump_privatekey(crypto.FILETYPE_PEM, k)
        ssh_keys.public_key = crypto.dump_publickey(crypto.FILETYPE_PEM, k)
        return ssh_keys

signals.post_save.connect(Member.post_save, sender=Member)