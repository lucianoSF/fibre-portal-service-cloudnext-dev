from mongoengine_goodjson import Document, EmbeddedDocument
from mongoengine import signals, StringField, BooleanField, LongField, ReferenceField, EmbeddedDocumentListField
from mongoengine.queryset.visitor import Q

from ..rest.errors import NotFoundError, BadRequestError
from .member import Member
from .project import Project
from .resource import Resource
from .image import Image
import logging
logger = logging.getLogger(__name__)

class HostImages(EmbeddedDocument):
    host = ReferenceField(Resource)
    image = ReferenceField(Image)

class Reservation(Document):
    starts_at = LongField() # timestamp
    ends_at = LongField() # timestamp
    enabled = BooleanField()
    host_images = EmbeddedDocumentListField(HostImages)
    project = ReferenceField(Project)
    created_by = ReferenceField(Member)

    @classmethod
    def pre_save(cls, sender, document, **kwargs):
        logger.debug(f"project: {document.project}")
        for host_image in document.host_images:
            logger.debug(f"host: {host_image.host}")
            conflicting = Reservation.objects((Q(ends_at__gte=document.starts_at) | Q(starts_at__lte=document.ends_at) | \
                Q(starts_at__gte=document.starts_at, ends_at__lte=document.ends_at) | Q(starts_at__lte=document.starts_at, ends_at__gte=document.ends_at)) & \
                Q(host_images__match={'host': host_image.host}))
            if conflicting: raise BadRequestError("Resources unavailable in this timeframe.")
        return
        

    @staticmethod
    def get_all():
        return Reservation.objects()

    @staticmethod
    def get(id):
        member = Reservation.objects(id=id).first()
        if not member: raise NotFoundError(f"Reservation with id = '{id}'not found")
        return member

signals.pre_save.connect(Reservation.pre_save, sender=Reservation)
