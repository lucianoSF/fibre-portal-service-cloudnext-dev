from mongoengine_goodjson import Document, EmbeddedDocument
from mongoengine import signals, StringField, LongField, EmbeddedDocumentListField, ReferenceField
from ..rest.errors import NotFoundError
from .member import Member
from datetime import datetime
import json, logging
logger = logging.getLogger(__name__)

participants_roles = ('basic', 'manager')
# ---- participants
class Participants(EmbeddedDocument):
    role = StringField(choices=participants_roles)
    member = ReferenceField(Member)

class Project(Document):
    name = StringField()
    creation_time = LongField(default=datetime.timestamp(datetime.now()))
    expiration_time = LongField() # timestamp
    description = StringField()
    owner = ReferenceField(Member)
    participants = EmbeddedDocumentListField(Participants)

    @classmethod
    def pre_save(cls, sender, document, **kwargs):
        for participant in document.participants:
            participant.member
        document.owner
    
    @staticmethod
    def get_all():
        return Project.objects().no_dereference()

    @staticmethod
    def get(id):
        project = Project.objects(id=id).first()
        if not project: raise NotFoundError(f"Project with id = '{id}'not found")
        return project
    
    @staticmethod
    def update_fields(project_id, fields_dict):
        project = Project.get(project_id)
        project.update(**fields_dict)
        project.save()
        project.reload()
        return Project.get(project_id)
    
    @staticmethod
    def is_owner(project_id, member_id):
        return Project.get(project_id).owner.id == member_id
    
    @staticmethod
    def is_manager(project_id, member_id):
        return Project.is_owner(project_id, member_id) or Project.objects(id=project_id, participants__match={'role': "manager", 'member': member_id}).no_dereference()
    
    @staticmethod
    def is_participant(project_id, member_id):
        return Project.is_owner(project_id, member_id) or Project.objects(participants__member=Member(id=member_id)).exclude("participants")

    @staticmethod
    def format_reference_obj(project):
        project = json.loads(project.to_json())
        project["owner"] = json.loads((Member.get(project["owner"])).to_json())
        for i in range(len(project["participants"])): project["participants"][i]["member"] = json.loads((Member.get(project["participants"][i]["member"])).to_json()) 
        return project

    @staticmethod
    def remove_participants(project_id, member_ids):
        for member_id in member_ids:
            Project.objects(id=project_id).update_one(pull__participants__member=Member(id=member_id))
        return Project.get(project_id)

signals.pre_save.connect(Project.pre_save, sender=Project)

