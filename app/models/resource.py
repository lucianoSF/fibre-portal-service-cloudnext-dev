from mongoengine_goodjson import Document
from mongoengine import signals, StringField, ListField, MapField, BooleanField, ReferenceField
from.resource_type import ResourceType

class Resource(Document):
    name = StringField()
    attributes = ListField(MapField(field=StringField()))
    enabled = BooleanField()
    resource_type = ReferenceField(ResourceType)

    @classmethod
    def pre_save(cls, sender, document, **kwargs):
        document.resource_type

signals.pre_save.connect(Resource.pre_save, sender=Resource)