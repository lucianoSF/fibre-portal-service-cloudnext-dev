from mongoengine_goodjson import Document
from mongoengine import signals, StringField, ListField, MapField

class ResourceType(Document):
    name = StringField()
    attributes = ListField(MapField(field=StringField())) 