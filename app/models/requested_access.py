from mongoengine.fields import BooleanField
from app.models.project import Project
from mongoengine_goodjson import Document
from mongoengine import signals, StringField, LongField, ReferenceField
from ..rest.errors import BadRequestError, NotFoundError
from .member import Member
from .project import Project, Participants, participants_roles
from datetime import datetime
import json, logging
logger = logging.getLogger(__name__)

class RequestedAccess(Document):
    member = ReferenceField(Member)
    project = ReferenceField(Project)
    # role = StringField(default='basic', choices=participants_roles) # apenas em approve
    message = StringField()
    creation_time = LongField(default=datetime.timestamp(datetime.now()))
    status = StringField(choices=('pending', 'denied', 'accepted'), default='pending')

    @classmethod
    def pre_save(cls, sender, document, **kwargs):
        document.member.id
        document.project.id
    
    @staticmethod
    def get_all():
        return RequestedAccess.objects(status='pending').no_dereference()
    
    @staticmethod
    def get_all_denied():
        return RequestedAccess.objects(status='denied').no_dereference()

    @staticmethod
    def get(id):
        req_acc = RequestedAccess.objects(id=id).no_dereference().first()
        if not req_acc: raise NotFoundError(f"RequestedAccess with id = '{id}' not found")
        return req_acc
    
    @staticmethod
    def get_all_from_member(member_id):
        Member.get(member_id)
        return RequestedAccess.objects(member=member_id).exclude("member").no_dereference()

    @staticmethod
    def get_all_from_project(project_id):
        Project.get(project_id)
        return RequestedAccess.objects(project=project_id).exclude("project").no_dereference()

    @staticmethod
    def format_reference_obj(req_acc):
        req_acc = json.loads(req_acc.to_json())
        if "member" in req_acc: req_acc["member"] = json.loads((Member.get(req_acc["member"])).to_json())
        if "project" in req_acc: req_acc["project"] = json.loads((Project.objects(id=req_acc["project"]).exclude("participants").first()).to_json())            
        return req_acc
    
    @staticmethod
    def approve(req_acc):
        req_acc_dict = json.loads(req_acc)
        req_acc = RequestedAccess.get(req_acc_dict['id'])
        if Project.is_participant(req_acc.project.id, req_acc.member.id): raise BadRequestError(f"Member '{req_acc.member.id}' already participates in project '{req_acc.project.id}'")
        if 'role' not in req_acc_dict: req_acc_dict['role'] = 'basic'
        if req_acc_dict['role'] not in participants_roles: raise BadRequestError(f"'{req_acc_dict['role']}' is not a valid role")
        Project.objects(id=req_acc.project.id).update_one(push__participants=Participants(role=req_acc_dict['role'], member=Member(id=req_acc.member.id)))
        req_acc.status = 'accepted'
        req_acc.save()
        return req_acc
    
    @staticmethod
    def deny(req_acc_id):
        req_acc = RequestedAccess.get(req_acc_id)
        req_acc.status = 'denied'
        req_acc.save()
        return req_acc

signals.pre_save.connect(RequestedAccess.pre_save, sender=RequestedAccess)

