from mongoengine import connect, NotUniqueError
from ..config import db_user, db_pass, db_host, db_port, db_dbase, root_login, root_pwd
import logging

logger = logging.getLogger(__name__)

def init_app(app):
    # database config
    connect(
        db=db_dbase,
        host=f"mongodb://{db_user}:{db_pass}@{db_host}:{db_port}/?authSource=admin"
    )
    populate_db()

def populate_db():
    from .member import Member
    try:
        root = Member(email=root_login, password=Member.hash_string(root_pwd), privilege=4)
        root.save()
        if not Member.objects(email=root_login): logger.error("Failed to create root user")
        else: logger.info("Created root user")
    except NotUniqueError:
        logger.info("Created root user")
        pass
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise
