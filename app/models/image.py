from mongoengine_goodjson import Document
from mongoengine import signals, StringField, ListField, MapField, BooleanField, ReferenceField
from .resource_type import ResourceType

class Image(Document):
    name = StringField()
    attributes = ListField(MapField(field=StringField()))
    # compatibles = ListField(ReferenceField(ResourceType)) #v1+
