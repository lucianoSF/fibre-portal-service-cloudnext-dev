from datetime import datetime

def ts_to_dt(timestamp):
    return datetime.fromtimestamp(timestamp)