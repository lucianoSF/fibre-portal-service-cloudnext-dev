import logging, traceback
logger = logging.getLogger(__name__)

def log_exc(err):
    return f"{err.__class__.__name__}: {err}"

def log_tb():
    logger.debug(traceback.format_exc())