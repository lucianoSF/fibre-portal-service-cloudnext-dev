from ..rest.errors import NotFoundError
import sys, logging, json

logger = logging.getLogger(__name__)

def parse_type(data_type, module):
    try:
        return getattr(sys.modules[module], data_type)
    except Exception:
        raise NotFoundError("Path not found.")

def get_all(data_type, module):
    try:
        obj_type = parse_type(data_type, module)
        method = getattr(obj_type, "get_all", None)
        if callable(method): return method()
        else: return obj_type.objects()
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

def get(data_id, data_type, module):
    try:
        obj_type = parse_type(data_type, module)
        method = getattr(obj_type, "get", None)
        if callable(method): return method(data_id)
        else: return obj_type.objects(id=data_id).first()
        if not data: raise NotFoundError(f"{obj_type().__class__.__name__} with id = '{data_id}' not found")
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise
        

def insert(json_data, data_type, module):
    try:
        obj_type = parse_type(data_type, module)
        new_data = obj_type(**json_data)
        new_data.save()
        return new_data
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

def update(json_data, data_id, data_type, module):
    try:
        data = get(data_id, data_type, module)
        data.from_json(json.dumps(json_data), created=True)
        data.save()
        return data
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

def delete(data_id, data_type, module):
    try:
        data = get(data_id, data_type, module)
        data.delete()
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise