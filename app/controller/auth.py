from ..config import jwt_secret_key, jwt_algorithm, jwt_exp_days, jwt_exp_seconds
import jwt, logging, datetime

logger = logging.getLogger(__name__)

def encode_auth_token(data,secret_key=jwt_secret_key):
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=jwt_exp_days,seconds=jwt_exp_seconds),
            'iat' : datetime.datetime.utcnow(),
            'data' : data
        }
        return jwt.encode(payload, secret_key, algorithm=jwt_algorithm)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

def decode_auth_token(auth_token,secret_key=jwt_secret_key):
    try:
        payload = jwt.decode(auth_token,secret_key, algorithms=jwt_algorithm)
        return payload['data']
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise