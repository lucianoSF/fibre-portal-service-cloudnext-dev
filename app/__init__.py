from flask import Flask
from flask_cors import CORS
from dotenv import load_dotenv
from .config import log_level
import logging

# init logs
logger = logging.getLogger(__name__)
f='%(asctime)s | %(levelname)s | %(name)s:%(funcName)s | %(message)s'
if log_level != logging.DEBUG: f='%(asctime)s | %(levelname)s | %(message)s'
logging.basicConfig(level=log_level, format=f)

def create_app():
    from . import models, rest
    app = Flask(__name__)
    CORS(app,  resources={r"/*": {"origins": "*"}})
    models.init_app(app)
    rest.init_app(app)
    return app        

if __name__ == '__main__':
    load_dotenv()
    app = create_app()
    app.run()

