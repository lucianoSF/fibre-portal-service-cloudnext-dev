from flask import Blueprint, request, g
from .response import response, pretty_obj
from .errors import ForbiddenError
from ..models.project import Project
from ..models.requested_access import RequestedAccess
import logging 

logger = logging.getLogger(__name__)
req_acc_bp = Blueprint('requested_accesses', '__name__', url_prefix='/requested_accesses')

@req_acc_bp.before_request
def check_privileges():
    def has_privilege():
        endpoint = request.path.split('/')[-1]
        if endpoint == 'all' or endpoint == 'details': return g.member_privilege >= 3
        if endpoint == 'create': return g.member_privilege >= 1
        if endpoint == 'update' or endpoint == 'remove': return g.member_privilege >= 3
        if endpoint == 'approve' or endpoint == 'deny': return g.member_privilege >= 1 and Project.is_manager(request.path.split('/')[2], g.member_id)
    if g.member_privilege >= 3 or has_privilege(): return
    else: raise ForbiddenError("Permission denied. You don't have the right privileges.")

# GET all 
@req_acc_bp.route('/all', methods=['GET']) 
def handle_all_requests():
    try:
        results = [RequestedAccess.format_reference_obj(project) for project in RequestedAccess.get_all()]
        return response(result=results)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

@req_acc_bp.route('/denied', methods=['GET']) 
def handle_all_denied_requests():
    try:
        results = [RequestedAccess.format_reference_obj(project) for project in RequestedAccess.get_all_denied()]
        return response(result=results)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise
    
    
# POST
@req_acc_bp.route('/create', methods=['POST']) 
def handle_post_request():
    try:
        req_acc = RequestedAccess(**request.get_json())
        req_acc.save()
        return response(message="Successfully created.", result=pretty_obj(req_acc), status_code=201)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise    

# GET
@req_acc_bp.route('/<req_acc_id>/details', methods=['GET'])
def handle_get_request(req_acc_id):
    try:
        project = RequestedAccess.get(req_acc_id)
        return response(result=RequestedAccess.format_reference_obj(project))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

@req_acc_bp.route('/approve', methods=['POST'])
def handle_approve_requests():
    try:
        req_acc = RequestedAccess.approve(request.get_json())
        return response(result=req_acc)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

@req_acc_bp.route('/deny', methods=['POST'])
def handle_deny_requests():
    try:
        req_acc = RequestedAccess.deny(request.get_json())
        return response(result=req_acc)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

