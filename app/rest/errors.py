from flask import Blueprint, request, jsonify
import logging, traceback

logger = logging.getLogger(__name__)
errors_bp = Blueprint('errors', '__name__')

@errors_bp.app_errorhandler(Exception)
def handle_errors(error):
    logger.debug(traceback.format_exc())
    status_code = _error_code.get(error.__class__.__name__)
    if not status_code: status_code = 500
    return jsonify({'error': str(error) }), status_code
# Custom Exceptions ----
class BadRequestError(Exception): pass
class NotFoundError(Exception): pass
class NotAuthorizedError(Exception): pass
class ForbiddenError(Exception): pass

# Exceptions Codes ----
_error_code ={
    'BadRequestError' : 400,
    'NotFoundError' : 404,
    'NotAuthorizedError' : 403,
    'ForbiddenError': 401,
    'ExpiredSignatureError': 401,
    'InvalidTokenError': 401,
    'InvalidSignatureError': 401,
    'MethodNotAllowed':405
}