from flask import Blueprint, request, g
from .response import response, pretty_obj
from .errors import ForbiddenError
from ..models.project import Project
from ..models.requested_access import RequestedAccess
import logging

logger = logging.getLogger(__name__)
project_bp = Blueprint('projects', '__name__', url_prefix='/projects')

@project_bp.before_request
def check_privileges():
    def has_privilege():
        endpoint = request.path.split('/')[-1]
        if endpoint == 'all' or endpoint == 'details' or endpoint == 'request_access': return g.member_privilege >= 1
        if endpoint == 'create': return g.member_privilege >= 2
        if endpoint == 'update' or endpoint == 'remove': return g.member_privilege >= 1 and Project.is_owner(request.path.split('/')[2], g.member_id)
        if endpoint == 'approve_requests': return g.member_privilege >= 1 and Project.is_manager(request.path.split('/')[2], g.member_id)
    if g.member_privilege >= 3 or has_privilege(): return
    else: raise ForbiddenError("Permission denied. You don't have the right privileges.")

# GET all 
@project_bp.route('/all', methods=['GET']) 
def handle_all_project():
    try:
        results = [Project.format_reference_obj(project) for project in Project.get_all()]
        return response(result=results)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise
    
# POST
@project_bp.route('/create', methods=['POST']) 
def handle_post_project():
    try:
        req = request.get_json()
        if "owner" not in req: req["owner"] = g.member_id
        if req["owner"] != g.member_id and g.member_privilege < 3: raise ForbiddenError("You don't have the right privileges to create a Project to another Member.")
        project = Project(**req)
        project.save()
        return response(message="Successfully created.", result=pretty_obj(project), status_code=201)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise    

# GET
@project_bp.route('/<project_id>/details', methods=['GET'])
def handle_get_project(project_id):
    try:
        project = Project.get(project_id)
        return response(result=Project.format_reference_obj(project))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise   

# PUT
@project_bp.route('/<project_id>/update', methods=['PUT'])
def handle_put_project(project_id):
    try:
        project = Project.update_fields(project_id, request.get_json())
        return response(message="Successfully updated.", result=pretty_obj(project))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

# DELETE
@project_bp.route('/<project_id>/remove', methods=['DELETE'])
def handle_delete_project(project_id):
    try:
        project = Project.get(project_id)
        project.delete()
        return response(message="Successfully deleted.")
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

@project_bp.route('/<project_id>/requested_accesses', methods=['GET'])
def handle_project_requested_accesses(project_id):
    try:
        result = [RequestedAccess.format_reference_obj(req_acc) for req_acc in RequestedAccess.get_all_from_project(project_id)]
        return response(result=pretty_obj(result))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

@project_bp.route('/<project_id>/remove_participants', methods=['POST'])
def handle_remove_participants(project_id):
    try:
        result = Project.remove_participants(project_id ,request.get_json())
        return response(result=pretty_obj(Project.format_reference_obj(result)))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise







