from flask import Blueprint, request, g
from .response import response, pretty_obj
from ..models.member import Member
from ..models.requested_access import RequestedAccess
from .errors import ForbiddenError
import logging, json

logger = logging.getLogger(__name__)
member_bp = Blueprint('members', '__name__', url_prefix='/members')

@member_bp.before_request
def check_privileges():
    endpoint = request.path.split('/')[-1]
    if endpoint == 'create' or request.path.split('/')[2] == g.member_id: return
    if g.member_privilege < 3: raise ForbiddenError("Permission denied. You don't have the right privileges.")

# GET all 
@member_bp.route('/all', methods=['GET']) 
def handle_all_member():
    try:
        results = Member.get_all()
        return response(result=pretty_obj(results))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise
    
    
# POST
@member_bp.route('/create', methods=['POST']) 
def handle_post_member():
    try:
        member = Member(**Member.validate_create(request.get_json()))
        member.save()
        member.ssh_keys = None
        return response(message="Successfully created.", result=pretty_obj(member), status_code=201)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise    

# GET
@member_bp.route('/<member_id>/details', methods=['GET'])
def handle_get_member(member_id):
    try:
        member = Member.get(member_id)
        return response(result=pretty_obj(member))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise   

# PUT
@member_bp.route('/<member_id>/update', methods=['PUT'])
def handle_put_member(member_id):
    try:
        if g.member_privilege < 3:
            if 'privilege' in request.get_json(): raise ForbiddenError("Permission denied. You don't have the right privileges to update 'privilege'.")
            if 'password' in request.get_json():  raise ForbiddenError("Permission denied. You can't update password in this endpoint.")
        member = Member.update_fields(member_id, request.get_json())
        return response(message="Successfully updated.", result=pretty_obj(member))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

# DELETE
# @member_bp.route('/<member_id>/remove', methods=['DELETE'])
# def handle_delete_member(member_id):
#     try:
#         member = Member.get(member_id)
#         member.delete()
#         return response(message="Successfully deleted.")
#     except Exception as err:
#         logger.error(f"{err.__class__.__name__}: {err}")
#         raise

@member_bp.route('/<member_id>/change_password', methods=['POST'])
def handle_member_change_password(member_id):
    try:
        data = request.get_json()
        Member.change_password(member_id, data['old_password'], data['new_password'])
        return response(message="Successfully changed password.")
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

@member_bp.route('/<member_id>/projects', methods=['GET'])
def handle_member_projects(member_id):
    try:
        results = Member.projects(member_id)
        return response(result=results)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

@member_bp.route('/<member_id>/requested_accesses', methods=['GET'])
def handle_member_requested_projects(member_id):
    try:
        result = [RequestedAccess.format_reference_obj(req_acc) for req_acc in RequestedAccess.get_all_from_member(member_id)]
        return response(result=result)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

@member_bp.route('/<member_id>/ssh_keys', methods=['GET'])
def handle_get_ssh_keys(member_id): 
    try:
        member_dict = json.loads(Member.get_ssh_keys(member_id).to_json())
        return response(result=member_dict['ssh_keys'])
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err} ")
        raise   






