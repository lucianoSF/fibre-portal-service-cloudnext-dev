from app.rest import members
from .auth import authorizes
from .json import is_json

def init_app(app):
    authorizes(app, skip_path=['/auth/login', '/members/create'])
    is_json(app)
