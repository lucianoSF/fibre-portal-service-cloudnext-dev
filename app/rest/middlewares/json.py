from flask import request
from ..errors import NotAuthorizedError

def is_json(app, skip_path=[]):
    @app.before_request
    def wrapper():
        if request.path in skip_path or (request.method != 'POST' and request.method != 'PUT'): return
        if not request.is_json: raise BadRequestError("The request payload is not in JSON format")