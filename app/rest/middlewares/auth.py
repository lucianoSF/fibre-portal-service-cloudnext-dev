from flask import request, g
from ...controller.auth import decode_auth_token
from ..errors import NotAuthorizedError, ForbiddenError
from ...models.member import Member
import logging

logger = logging.getLogger(__name__)

def authorizes(app, skip_path=[]):
    @app.before_request
    def wrapper():
        if request.path in skip_path: return
        if request.method == 'OPTIONS': return '', 200
        auth = request.headers.get("Authorization")
        if not auth:  raise NotAuthorizedError("Missing 'Authorization' header")
        g.member_id = decode_auth_token(auth)['member_id']
        try:
            member = Member.get(g.member_id)
            if not member.enabled: raise NotAuthorizedError("Token user is disabled")
            g.member_privilege = member.privilege
        except Exception as err:
            logger.error(f"{err.__class__.__name__}: {err}")
            raise ForbiddenError("Invalid token. Please login again.")


# Member privilegies level:
# 0 - Disabled
# 1 - Basic member
# 2 - Full member
# 3 - Root