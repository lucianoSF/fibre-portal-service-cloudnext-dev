from flask import jsonify
import logging, json

logger = logging.getLogger(__name__)

def response(status_code=200, **content):
    if not content: return ('', 204)
    return content, status_code

def pretty_obj(obj):
    try:
        if isinstance(obj, list):
            results = []
            for result in obj:
                if not isinstance(result, dict): results.append(json.loads(result.to_json()))
                else: results.append(result)
            return results
        elif not isinstance(obj, dict): return json.loads(obj.to_json())
        else: return obj
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise

# Code definitions ----
# 200 OK
# 201 Created   (new data)
# 202 Accepted  (async)
# 204 No Content