from flask import Blueprint, request
from .errors import NotFoundError, NotAuthorizedError
from .response import response, pretty_obj
from ..models.member import Member
from ..controller.auth import encode_auth_token
import json, bcrypt, logging

logger = logging.getLogger(__name__)
auth_bp = Blueprint('auth', '__name__', url_prefix='/auth')

# POST
@auth_bp.route('/login', methods=['POST']) 
def handle_login():
    try:
        data = json.loads(json.dumps(request.get_json()))
        member = Member.objects(email=data['email']).first()
        if not member: raise NotFoundError(f"Member with email={data['email']} no found")
        if not member.enabled: raise NotAuthorizedError("Token user is disabled")
        if not bcrypt.checkpw(data['password'].encode('utf-8'), member.password.encode('utf-8')): raise NotAuthorizedError("Password does not match.")

        token_data = {'member_id': str(member.id)}
        auth_token = encode_auth_token(data=token_data)

        member.password = None
        return response(message="Successfully logged in.", result={"auth_token": auth_token ,"member": pretty_obj(member)})
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise
