from flask import Blueprint, request, g
from .response import response, pretty_obj
from ..controller import database as db
from .errors import ForbiddenError
# ---- import models Class as resource_type endpoint ----
from ..models.resource_type import ResourceType as resource_types
from ..models.resource import Resource as resources
from ..models.image import Image as images

resources_bp = Blueprint('resources', '__name__', url_prefix='/<resource_type>')

@resources_bp.before_request
def check_privileges():
    def has_privilege():
        endpoint = request.path.split('/')[-1]
        if endpoint == 'all' or endpoint == 'details': return g.member_privilege >= 1
        if endpoint == 'create' or endpoint == 'update' or endpoint == 'remove': return g.member_privilege >= 3
    if g.member_privilege >= 3 or has_privilege(): return
    else: raise ForbiddenError("Permission denied. You don't have the right privileges.")

# GET all 
@resources_bp.route('/all', methods=['GET']) 
def handle_all_data(resource_type):
    results = db.get_all(resource_type, __name__)
    return response(result=pretty_obj(results))
    
# POST
@resources_bp.route('/create', methods=['POST']) 
def handle_post_data(resource_type):
    new_data = db.insert(request.get_json(), resource_type, __name__)
    return response(message="Successfully created.", result=pretty_obj(new_data), status_code=201)

# GET
@resources_bp.route('/<resource_id>/details', methods=['GET', 'PUT', 'DELETE'])
def handle_get_data(resource_type, resource_id):
    data = db.get(resource_id, resource_type, __name__)
    return response(result=pretty_obj(data))

# PUT
@resources_bp.route('/<resource_id>/update', methods=['PUT'])
def handle_put_data(resource_type, resource_id):
    data = db.update(request.get_json(), resource_id, resource_type, __name__)
    return response(message="Successfully updated.", result=pretty_obj(data))

# DELETE
@resources_bp.route('/<resource_id>/remove', methods=['DELETE'])
def handle_delete_data(resource_type, resource_id):
    db.delete(resource_id, resource_type, __name__)
    return response(message="Successfully deleted.")






