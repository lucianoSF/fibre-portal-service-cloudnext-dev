from flask import Blueprint, request, g
from .response import response, pretty_obj
from .errors import ForbiddenError
from ..models.reservation import Reservation
import logging

logger = logging.getLogger(__name__)
reservation_bp = Blueprint('reservations', '__name__', url_prefix='/reservations')

@reservation_bp.before_request
def check_privileges():
    def has_privilege():
        endpoint = request.path.split('/')[-1]
        return True
        if endpoint == 'all' or endpoint == 'details' or endpoint == 'request_access': return g.member_privilege >= 1
        if endpoint == 'create': return g.member_privilege >= 2
        if endpoint == 'update' or endpoint == 'remove': return g.member_privilege >= 1 and Reservation.is_owner(request.path.split('/')[2], g.reservation_id)
        if endpoint == 'approve_requests': return g.member_privilege >= 1 and Reservation.is_manager(request.path.split('/')[2], g.reservation_id)
    if g.member_privilege >= 3 or has_privilege(): return
    else: raise ForbiddenError("Permission denied. You don't have the right privileges.")

# GET all 
@reservation_bp.route('/all', methods=['GET']) 
def handle_all_reservation():
    try:
        results = Reservation.get_all()
        return response(result=pretty_obj(results))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise
    
    
# POST
@reservation_bp.route('/create', methods=['POST']) 
def handle_post_reservation():
    try:
        reservation = Reservation(**request.get_json())
        reservation.save()
        return response(message="Successfully created.", result=pretty_obj(reservation), status_code=201)
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise    

# GET
@reservation_bp.route('/<reservation_id>/details', methods=['GET'])
def handle_get_reservation(reservation_id):
    try:
        reservation = Reservation.get(reservation_id)
        return response(result=pretty_obj(reservation))
    except Exception as err:
        logger.error(f"{err.__class__.__name__}: {err}")
        raise   

# PUT
# @reservation_bp.route('/<reservation_id>/update', methods=['PUT'])
# def handle_put_reservation(reservation_id):
#     try:
#         if g.member_privilege < 3:
#             if 'privilege' in request.get_json(): raise ForbiddenError("Permission denied. You don't have the right privileges to update 'privilege'.")
#             if 'password' in request.get_json():  raise ForbiddenError("Permission denied. You can't update password in this endpoint.")
#         member = Reservation.update_fields(reservation_id, request.get_json())
#         return response(message="Successfully updated.", result=pretty_obj(member))
#     except Exception as err:
#         logger.error(f"{err.__class__.__name__}: {err}")
#         raise

# DELETE
# @reservation_bp.route('/<reservation_id>/remove', methods=['DELETE'])
# def handle_delete_reservation(reservation_id):
#     try:
#         member = Reservation.get(reservation_id)
#         member.delete()
#         return response(message="Successfully deleted.")
#     except Exception as err:
#         logger.error(f"{err.__class__.__name__}: {err}")
#         raise