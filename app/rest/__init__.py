from .resources import resources_bp
from .auth import auth_bp
from .errors import errors_bp
from .projects import project_bp
from .members import member_bp
from .requested_accesses import req_acc_bp
from .reservation import reservation_bp

def init_app(app):
    from . import middlewares
    middlewares.init_app(app)

    app.register_blueprint(errors_bp)
    app.register_blueprint(auth_bp)
    app.register_blueprint(member_bp)
    app.register_blueprint(project_bp)
    app.register_blueprint(req_acc_bp)
    app.register_blueprint(reservation_bp)
    app.register_blueprint(resources_bp)



