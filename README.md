# python-orm
Testing python ORMs and DBs 

## Setup configuration
create .env file.
Structure:
```
# ------ MONGODB CONFIG ------
MONGODB_HOST=               # docker host
MONGODB_PORT=               # docker port
MONGODB_USER=
MONGODB_PASSWORD=
MONGODB_DATABASE=

# ------ JWT CONFIG ------
JWT_SECRET_KEY=secret_key
JWT_ALGORITHM=HS256
JWT_EXPIRATION_DAYS=30
JWT_EXPIRATION_SECONDS=0

# ------ APP CONFIG ------
LOG_LEVEL=debug
ROOT_LOGIN=insert-your-root-user-here@portal.com
ROOT_PASSWORD=insert-your-root-password-here
```

## start app
```
docker-compose up -d 
```

## logging 
```
docker logs -f orm-app
```

## development
First config:
```
python3 -m venv env
source env/bin/activate
pip3 install -r ./app/requirements.txt
```

Update ´requirements.tx´:
```
source env/bin/activate
pip3 install package
pip3 freeze > requirements.txt
```

Update docker image:
```
docker-compose up --build -d
```

Import logger in module (file):
```
import logging
logger = logging.getLogger(__name__)
```
