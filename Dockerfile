FROM python:3.8-slim-buster

WORKDIR /app

#RUN apt-get install -y libssl-dev libffi-dev

# install requirements
COPY ./app/requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# copy app files
COPY ./app .
COPY ./.env .

# copy test files except populate_test_objects.py
COPY ./test ./test
RUN rm test/populate_test_objects.py

# export run file
ENV FLASK_APP=/app/__init__.py
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
